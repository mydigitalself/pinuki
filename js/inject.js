function injectPinnedClass(headroom) {
  document.body.className += " pinuki-pinned"
  if (headroom) {
    var myElement = document.querySelector("header");
    var headroom  = new Headroom(myElement, {
      tolerance : {
        down: 10,
        up: 20
      }
    });
    headroom.init();
  }
}

chrome.storage.sync.get({
  gitlabUri: 'https://gitlab.com',
  headroom: true
}, function(items) {
    if (location.href.includes(items.gitlabUri)) {
      injectPinnedClass(items.headroom);
    }
});
